PRODUCT_PACKAGES += \
    AndroidMediaShell \
    AtvRemoteService \
    BugReportSender \
    CanvasPackageInstaller \
    FrameworkPackageStubs \
    GoogleBackupTransport \
    GoogleCalendarSyncAdapter \
    GoogleContactsSyncAdapter \
    GooglePartnerSetup \
    Pumpkin \
    TV